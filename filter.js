function filter(array,cb) {
    if(array.length === 0 ){
        return []
    } else {
        let result = []
        for (let index = 0; index < array.length; index++) {
            const element = array[index];
            if (cb(element,index,array) === true ) {
                result.push(element)
            }
        }
        return result
    }
}

module.exports = filter 