function flatten(myArray) {
    if (myArray.length === 0 || typeof(myArray) === 'undefined') {
        return []
    }
    flattenArray = []
    for(let i = 0 ; i < myArray.length ; i++ ) {
        let element = myArray[i]
        Array.isArray(element) ? flattenArray = flattenArray.concat(flatten(element)) : flattenArray.push(element)
    }
    return flattenArray
}

module.exports = flatten