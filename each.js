function each(items, cb) {
    if (items.length === 0) {
        console.log('There are no items found. Please provide data!')
    }
    else if (Array.isArray(items)) {
        for (let i = 0 ; i < items.length ; i++ ) {
            cb(i,items[i])
        }
    } else {
        console.log("Please enter valid data")
    }
}

module.exports = each ;