function find(array,cb){
    if (array.length === 0 ) {
        return
    }
    else {
        for (let index = 0; index < array.length; index++) {
            const element = array[index];
            if(cb(element)) {
                return element
                break
            }
        }
        return 
    }
}

module.exports = find 