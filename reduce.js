function reduce(items,cb, acc) {
    if (items.length === 0 || !(Array.isArray(items))) {
        return []
    } else if (Array.isArray(items) && typeof(acc) !== 'undefined'){
        let result = acc
        for (let index = 0; index < items.length; index++) {
            const element = items[index];
            let value = cb(result,element,items)
            result = value
        }
        return result;
    } else if (Array.isArray(items) && typeof(acc) === 'undefined'){
        let result = items[0]
        for (let index = 1; index < items.length; index++) {
            const element = items[index];
            let value = cb(result,element,items)
            result = value
        }
        return result;
    } 
}

module.exports = reduce ;