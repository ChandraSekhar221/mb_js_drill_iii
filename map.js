function map(items,cb) {
    if (items.length === 0) {
        return []
    }else if (Array.isArray(items)) {
        let newArray = []
        for (let i = 0; i < items.length ; i++) {
            let mappedvalue = cb(items[i],i,items)
            newArray.push(mappedvalue)
        }
        return newArray ;
    } else {
        return []
    }
}

module.exports = map ;